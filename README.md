# apk-firebase-data-extractor

extract firebase credentials (and a bit more) from an APK file

MRs are welcome

requirements:

- [python3](https://www.python.org/)
- [apktool](https://ibotpeaches.github.io/Apktool/)
- [apksigner](https://developer.android.com/studio/command-line/apksigner.html)

chat on matrix: [#erupcja-apk-firebase-data-extractor:laura.pm](https://matrix.to/#/#erupcja-apk-firebase-data-extractor:laura.pm) / [#erupcja:laura.pm](https://matrix.to/#/#erupcja:laura.pm)
